package com.mevg.lalo.eva2_1_sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    TextView tvDatos;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvDatos = (TextView) findViewById(R.id.datos);
        db = openOrCreateDatabase("data", MODE_PRIVATE, null);
        //Crear una tabla -> genera una excepcion si la tabla existe
        //File dbPath = getApplication().getFilesDir();
        //String path = dbPath + "/" + "otraRuta";
        //db = openDatabase(path, null, SQLiteDatabase.CREATE_ID_NECESSARY)
        db.execSQL("create table if not exists amigos( " +
                   "id integer primary key autoincrement, " +
                   "name text, " +
                   "phone text )");
        db.execSQL("insert into amigos (name, phone) " +
                   "values ('lalo', '6141234567')");
        db.execSQL("insert  into amigos (name, phone) " +
                "values ('ana', '6147654321')");
        db.execSQL("insert into amigos (name, phone) " +
                "values ('alan', '6141726354')");
        db.execSQL("insert into amigos (name, phone) " +
                "values ('majo', '6147162534')");
        db.execSQL("insert into amigos (name, phone) " +
                "values ('gordita', '6149876543')");
        db.execSQL("insert into amigos (name, phone) " +
                "values ('ana lo hizo', '6143456789')");

        Cursor stm = db.rawQuery("select * from amigos", null);
        tvDatos.setText("");
        stm.moveToFirst();
        while (!stm.isAfterLast()){
            int id = stm.getInt(0);
            String name = stm.getString(1);
            String phone = stm.getString(2);
            tvDatos.append("id: " +id+"\n"+
                           "name: " + name+"\n"+
                           "phone: " +phone +"\n" );
            stm.moveToNext();
        }

    }
}
